<?php

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_Config extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->master_facility();
        $this->condition();
        $this->consituency();
        $this->sub_county();
        $this->gender();
        $this->groups();
        $this->language();
        $this->message_types();
        $this->marital_status();
        $this->module();
        $this->partner_facility();
        $this->time();
        $this->role();
    }

    public function master_facility() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_master_facility")->result();
        foreach ($get_partner_facility as $value) {
            $code = $value->code;
            $id = $value->id;
            $name = $value->name;
            $reg_number = $value->reg_number;
            $keph_level = $value->keph_level;
            $facility_type = $value->facility_type;
            $owner = $value->owner;
            $regulatory_body = $value->regulatory_body;
            $beds = $value->beds;
            $cots = $value->cots;
            $is_approved = $value->county_id;
            $reason = $value->consituency_id;
            $Sub_County_ID = $value->Sub_County_ID;
            $Ward_id = $value->Ward_id;
            $operational_status = $value->operational_status;
            $Open_whole_day = $value->Open_whole_day;
            $Open_public_holidays = $value->Open_public_holidays;
            $Open_weekends = $value->Open_weekends;
            $Open_late_night = $value->Open_late_night;
            $Service_names = $value->Service_names;
            $Approved = $value->Approved;
            $Public_visible = $value->Public_visible;
            $Closed = $value->Closed;
            $assigned = $value->assigned;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $lat = $value->lat;
            $lng = $value->lng;
            $county_id = $value->county_id;
            $consituency_id = $value->consituency_id;

            $check_existence = $this->db->query("Select * from tbl_master_facility where code like '%$code%'");
            if ($check_existence->num_rows() > 0) {
              
                   //Exists
                   echo 'Exists...'.$id.'<br>';
                $this->db->trans_start();


                $data = array(
                    'ushauri_id' => $id
                );
                $this->db->where('code', $code);
                $this->db->update('master_facility', $data);
    
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    
                } else {
                    
                }


            } else {
                echo 'Does not exist <br>';   
            }
       


           
        }
    }

    function escape_output($string) {
        $newString = str_replace('\r\n', '<br/>', $string);
        $newString = str_replace('\n\r', '<br/>', $newString);
        $newString = str_replace('\r', '<br/>', $newString);
        $newString = str_replace('\n', '<br/>', $newString);
        $newString = str_replace('\'', '', $newString);
        return $newString;
    }

    public function consituency() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_consituency")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $name = $this->escape_output($name);
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_consituency where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...'.$id.'<br>';
                $this->db->trans_start();


                $data = array(
                    'ushauri_id' => $id
                );
                $this->db->where('name', $name);
                $this->db->update('tbl_consituency', $data);
    
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    
                } else {
                    
                }
            } else {
                echo 'Does not exist <br>';
            }
          


           
        }
    }

    public function sub_county() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_sub_county")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $name = $this->escape_output($name);
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_sub_county where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_sub_county', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function gender() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_gender")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_gender where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_gender', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function condition() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_condition")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_condition where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_condition', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function groups() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_groups")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_groups where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_groups', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function language() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_language")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_language where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_language', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function message_types() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_message_types")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_message_types where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_message_types', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function marital_status() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_marital_status")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $marital = $value->marital;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_marital_status where marital like '%$marital%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('marital', $marital);
            $this->db->update('marital_status', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function module() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_module")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $module = $value->module;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;
            $level = $value->level;
            $order = $value->order;
            $controller = $value->controller;
            $function = $value->function;
            $status = $value->status;
            $description = $value->description;
            $span_class = $value->span_class;
            $icon_class = $value->icon_class;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $partner_id = $value->partner_id;


            $check_existence = $this->db->query("Select * from tbl_module where module like '%$module%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('module', $module);
            $this->db->update('tbl_module', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function partner_facility() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_partner_facility")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $mfl_code = $value->mfl_code;
            $coutny_id = $value->county_id;
            $sub_county_id = $value->sub_county_id;
            $is_approved = $value->is_approved;
            $reason = $value->reason;
            $avg_clients = $value->avg_clients;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_partner_facility where mfl_code like '%$mfl_code%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('mfl_code', $mfl_code);
            $this->db->update('tbl_partner_facility', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function time() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_time")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_time where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_time', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

    public function role() {
        $ushauri_db = $this->load->database('ushauri_db', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.

        $get_partner_facility = $ushauri_db->query("Select * from tbl_role")->result();
        foreach ($get_partner_facility as $value) {
            $status = $value->status;
            $id = $value->id;
            $name = $value->name;
            $created_by = $value->created_by;
            $updated_by = $value->updated_by;
            $created_at = $value->created_at;
            $updated_at = $value->updated_at;

            $check_existence = $this->db->query("Select * from tbl_role where name like '%$name%'");
            if ($check_existence->num_rows() > 0) {
                //Exists
                echo 'Exists...<br>';
            } else {
                
            }
            echo 'Does not exist <br>';


            $this->db->trans_start();


            $data = array(
                'ushauri_id' => $id
            );
            $this->db->where('name', $name);
            $this->db->update('tbl_role', $data);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                
            } else {
                
            }
        }
    }

}
